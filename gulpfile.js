/*
Instalar Gulp globalmente: npm install --global gulp-cli

Iniciar proyecto Node: npm init
Instalar dependencias: npm install --save-dev gulp gulp-sass gulp-autoprefixer gulp-pug gulp-rimraf gulp-connect connect-history-api-fallback
*/
'use strict'

var		gulp = require('gulp'),
		sass = require('gulp-sass'),
		autoprefixer = require('gulp-autoprefixer'),
		pug = require('gulp-pug'),
		clean = require('gulp-rimraf'),
		connect = require('gulp-connect'),
		historyApiFallback = require('connect-history-api-fallback')

gulp.task('sass', ()=>
	gulp.src('./scss/*.scss')
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(autoprefixer({
			versions: ['last 3 browsers']
		}))
		.pipe(gulp.dest('./app/css'))
)

gulp.task('pug', () => 
	gulp.src('./pug/*.pug')
		.pipe(pug({
			pretty: false
		}))
		.pipe(gulp.dest('./app'))
)

gulp.task('clean', ['pug', 'sass'], () => 
	gulp.src(['./app/components'], { read: false })
		.pipe(clean({
			force: true
		}))
)

gulp.task('prepros', () => {
	gulp.watch('./pug/**/*.*', ['clean']);
	gulp.watch('./scss/**/*.*', ['clean']);
	gulp.watch('./app/js/**/*.*', ['clean']);
})


gulp.task('server', function(){
	connect.server({
		root:'./app',
		port:'3000',
		livereload: true,
		middleware: function(connect,opt){
			return[historyApiFallback({})]
		}
	})
})


gulp.task('reload',function(){
	gulp.src('./app/**/**/*.*').pipe(connect.reload())
})

gulp.task('watch',function(){
	gulp.watch(['./app/**/**/*.*'],['reload'])
})

gulp.task('default', ['prepros','server', 'watch'])